# Introdução

**Blockchain** é uma das tecnologias mais revolucionárias do século 21, que ainda está amadurecendo e cujo potencial ainda não está totalmente concretizado. Em sua essência, *blockchain* é apenas um banco de dados de distribuição de registros. Mas o que o torna único é que não é um banco de dados privado, mas público, ou seja, todos que o usam têm uma cópia completa ou parcial dele. E um novo registro pode ser adicionado apenas com o consentimento de outros responsáveis ​​pelo banco de dados. Além disso, é o *blockchain* que faz a cryptocurrencies* e contratos inteligentes possíveis.

> **Cryptocurrency**: é uma versão digital do dinheiro, onde as transações são feitas on-line.

Nesta série de artigos, criaremos uma criptomoeda simplificada baseada em uma implementação simples de *blockchain*.

# Block (Bloco)

Vamos começar com a parte **block** de *blockchain*.

No *blockchain*, são os *Blocks* (blocos) que armazenam informações valiosas. Por exemplo, *block* de bitcoin armazenam transações, a essência de qualquer criptomoeda*.

> **Criptomoeda**: é um tipo de moeda virtual que utiliza a criptografia para garantir mais segurança em transações financeiras na internet.

Além disso, um *block* contém algumas informações técnicas, como sua **versão**, **timestamp atual** e o **hash do block anterior**.

Neste artigo, não vamos implementar o *block* como descrito nas especificações *blockchain* ou Bitcoin. Em vez disso, usaremos uma versão simplificada dele, que contém apenas informações significativas.

```golang 
  // Block - estrutura de um block
  type Block struct {
    Timestamp     int64
    Data          []byte
    PrevBlockHash []byte
    Hash          []byte  
  }
```

**Timestamp** é o registro de data e hora atual (quando o *block* é criado).
**Data** é a informação valiosa real que contém no bloco.
**PrevBlockHash** armazena o hash do *block* anterior.
**Hash** é o hash do *block*.

Na especificação de Bitcoint *Timestamp*, *PrevBlockHash* e *Hash* são cabeçalhos de *block*, que formam uma estrutura de dados separada, e a transações (**Data** no nosso caso) são uma estrutura de dados separado. Vamos misturando aqui para simplificar.

Então, como calculamos os *hashes*?

A forma como os hashes são calculadas é uma característica muito importante do *blockchain*, e é esse recurso que torna o *blockchain* **seguro**. A questão é que calcular um hash é uma operação computacionalmente difícil, leva algum tempo até para computadores rápidos (**é por isso que as pessoas compram CPUs poderosas para minerar* Bitcoin**). 

> O processo de **minerar** consiste em iteradamente modificar o conteúdo do bloco até que ele gere um hash que atende ao requisito da prova de esforço.

Esse é um projeto arquitetônico intencional, o que dificulta a adição de novos *blocks*, impedindo a modificação deles depois que eles são adicionados. Vamos discutir e implementar esse mecanismo em um artigo futuro.

Por enquanto, vamos apenas pegar campos de bloco, concatená-los e calcular um *hash* **SHA-256*** na combinação concatenada. 


> SHA-2 é um conjunto de funções hash criptográficas projetadas pela NSA (Agência de Segurança Nacional dos EUA). SHA significa secure hash algorithm (algoritmo de hash seguro). **Funções hash criptográficas** são operações matemáticas executadas em dados digitais; comparando o hash computado (a saída de execução do algoritmo) a um valor de hash conhecido e esperado, uma pessoa pode determinar a integridade dos dados. **SHA-256**: é uma função criptográfica utilizada como base do sistema de trabalho do bitcoin. SHA-256 e SHA-512 são funções hash inovadoras computadas com palavras de 32 bytes e 64 bytes, respectivamente. Eles usam quantidades de deslocamento e constantes aditivas diferentes, mas as suas estruturas são praticamente idênticas, diferindo apenas no número de rodadas. 

Vamos fazer isso no método SetHash:


```golang
  // SetHash: Concatenando as informações do block e calcula a hash.
  func (b *Block) SetHash() {
    // timestamp é um slice de byte de uma string formatada na base 10
    // de um int64
    timestamp := []byte(strconv.FormatInt(b.Timestamp, 10))
    // headers é um slice de byte concatenados com os elemetos passados ao ponteiro do Block 
    headers := bytes.Join([][]byte{b.PrevBlockHash, b.Data, timestamp}, []byte{})
    /*
      Sum256 retorna a soma de verificação SHA256 dos dados.
    */
    hash := sha256.Sum256(headers)
    // Atribue ao elemento Hash o mesmo slice passado na soma de verificação
	  b.Hash = hash[:]
  }
```

Em seguida, seguindo uma convenção de **Golang**, implementaremos uma função que simplificará a criação de um bloco:

```golang
  // NewBlock - Criando e retornando um ponteiro para Block
func NewBlock(data string, prevBlockHash []byte) *Block {
  // passamos para a variavel o ponteiro da mémoria &Block
	block := &Block{
		time.Now().Unix(),// retorna a hora local correspondente ao tempo Unix
		[]byte(data), // cria um slice de byte da string data
		prevBlockHash,
		[]byte{}, // cria um slice de byte vazio
	}
  // calcula e cria a hash 
	block.SetHash() 

	return block
}
```

# Blockchain

Agora vamos implementar um **blockchain**. Em sua essência, o *blockchain* é apenas um banco de dados com determinada estrutura: *é uma lista ordenada e vinculada*. O que significa que os blocos são armazenados no pedido de inserção e que cada bloco está vinculado ao anterior. Essa estrutura permite obter rapidamente o bloco mais recente em uma cadeia e obter (de maneira eficiente) um bloco pelo hash.

Em **Golang**, essa estrutura pode ser implementada usando uma array e um map: o array manterá os hashes ordenados (os arrays são ordenados em Go) e o map manterá **hash → pares de blocks (blocos)** (os maps não são ordenados). Mas, para o nosso protótipo de *blockchain*, usaremos apenas um array, porque não precisamos obter blocks com o hash deles por enquanto.

```golang 
 type Blockchain struct {
   // Criamos um ponteiro para um Array de Block
   blocks []*Block
 }
```

Agora vamos permitir adicionar blocos ao **Blockchain**:

```golang 
  // AddBlock - Adicionando um block dentro do Blockchain
  func (bc *Blockchain) AddBlock(data string) {
    //prevBlock - receber o ultimo block adicionando
    prevBlock := bc.blocks[len(bc.blocks)-1]
    //newBlock - contrue um novo block
    newBlock := NewBlock(data, prevBlock.prevBlockHash)
    //bc.blocks - (append-acrescenta elementos ao array)
    //append('o elemento que vai receber', 'elemento que vai ser passado')
    //passando o elemento para o blocks 
    bc.blocks = append(bc.blocks, newBlock)
  }
```

Para adicionar um novo block, precisamos de um block existente, mas não há blocks no nosso *blockchain*. Assim, em qualquer blockchain, deve haver pelo menos um block, e esse block, o primeiro da cadeia, é chamado de **Genesis Block***. 

> Um *Genesis Block* é o primeiro block de uma cadeia de blocks.

Vamos implementar um método que crie esse block:

```golang
  //NewGenesisBlock - Criando o block inicial e retornando um ponteiro para Block
  func NewGenesisBlock() *Block {
    return NewBlock("Genesis Block", []byte{})
  }
```

Agora, podemos implementar uma função que cria um *blockchain* com o block genesis:

```golang 
  func NewBlockchain() *Blockchain {
    return &Blockchain{[]*Block{NewGenesisBlock()}}
  }
```

Agora vamos verificar se o blockchain funciona corretamente:

```golang 
func main() {
  // Criando uma variavel com ponteiro da memoria para o Blockchain
  bc := NewBlockchain()
  // Pegando a variavel e adicionando um novo block
	bc.AddBlock("Enviando 1 BTC para Leonardo")
  bc.AddBlock("Enviando 2 BTC para Diogo")
  // Percorrendo o array criando com os block
	for _, block := range bc.blocks {
    // Imprimindo no terminal
		fmt.Printf("Prev. hash: %x\n", block.PrevBlockHash)
		fmt.Printf("Data: %s\n", block.Data)
		fmt.Printf("Hash: %x\n", block.Hash)
		fmt.Println("------------------------------------")
	}
}
```

#Conclusão

Construímos um protótipo **blockchain** muito simples: 

  * Que apenas uma matriz de blocos, com cada bloco tendo uma conexão com o anterior.

O **blockchain** real é muito mais complexo. Em nosso blockchain, adicionar novos blocos é fácil e rápido, mas no blockchain real a adição de novos blocos requer algum trabalho: 

* é preciso executar alguns cálculos pesados ​​antes de obter permissão para adicionar blocks *(esse mecanismo é chamado de **Proof-of-Work (Prova de Trabalho)** )*. Além disso, o blockchain é um banco de dados distribuído que não possui um único tomador de decisão.

Assim, um novo block deve ser confirmado e aprovado por outros participantes da rede *(esse mecanismo é chamado de consensus)*. E ainda não há transações no nosso blockchain!