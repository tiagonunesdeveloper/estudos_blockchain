# Introdução

 Até agora, construímos um *blockchain* com um sistema de proof-of-work *(prova de trabalho)*, o que torna a mineração possível. Nossa implementação está se aproximando de um *blockchain* totalmente **funcional**, mas ainda falta alguns recursos importantes. Hoje começaremos a armazenar um *blockchain* em um **banco de dados** e, depois disso, criaremos uma interface de linha de comando simples para executar operações com o *blockchain*.

 Atualmente, não há banco de dados em nossa implementação; em vez disso, criamos blocks toda vez que executamos o programa e os armazenamos na memória. Não podemos reutilizar um *blockchain*, não podemos compartilhá-lo com outras pessoas, portanto, precisamos armazená-lo no disco.

Qual banco de dados precisamos? Na verdade, qualquer um deles. No artigo original do Bitcoin, nada é dito sobre o uso de um determinado banco de dados, então cabe a um desenvolvedor usar o banco de dados. O Bitcoin Core, que foi inicialmente publicado por Satoshi Nakamoto e que atualmente é uma implementação de referência do Bitcoin, usa o **LevelDB** (embora tenha sido apresentado ao cliente apenas em 2012).


# BoltDB

Porque usar o **BoltDB**:

  1. É simples e minimalista.
  2. É implementado em Go.
  3. Não é necessário executar um servidor.
  4. Permite construir a estrutura de dados que queremos.

Do README do BoltDB no Github:

> **Bolt** é uma puro store de chave/valor de Go inspirada no projeto *LMDB* ( O banco de dados mapeado por memória (LMDB) do Lightning que é uma biblioteca de software que fornece um banco de dados transacional incorporado de alto desempenho na forma de um armazenamento de valor-chave.) de Howard Chu. O objetivo do projeto é fornecer um banco de dados simples, rápido e confiável para projetos que não exigem um servidor de banco de dados completo, como o Postgres ou o MySQL. Como o Bolt deve ser usado como uma funcionalidade de baixo nível, a simplicidade é a **chave**. A API será pequena e focará apenas na obtenção de valores e valores de configuração.

Soa perfeito para as nossas necessidades! Vamos passar um minuto revendo.

O *BoltDB* é um armazenamento de **chave/valor**, o que significa que não há tabelas como no *SQL RDBMS (MySQL, PostgreSQL, etc.)*, sem linhas, sem colunas. Em vez disso, os dados são armazenados como pares de valores-chave *(como nos mapas de Golang)*. Os pares de valores-chave são armazenados em intervalos, destinados a agrupar pares semelhantes *(isso é semelhante a tabelas no RDBMS*)*. Assim, para obter um valor, você precisa conhecer um bloco e uma chave.

> RDBMS é um banco de dados que modela os dados de uma forma que eles sejam percebidos pelo usuário como tabelas, ou mais formalmente relações.

Uma coisa importante sobre o *BoltDB* é que não há tipos de dados:* chaves e valores são matrizes de bytes*. Como armazenaremos estruturas do Go *(em particular, block)*, precisaremos serializá-las, ou seja, implementar um mecanismo de conversão de uma estrutura do Go em um array de bytes e restaurá-lo de volta de um array de bytes. Usaremos *encoding/gob* para isso, mas **JSON, XML, Protocol Buffers, etc**. também podem ser usados. Estamos usando *encoding/gob* porque é simples e faz parte da biblioteca padrão do Go.

# Estrutura do Banco de Dados

Antes de começar a implementar a **lógica de persistência**, primeiro precisamos decidir como vamos armazenar dados no banco de dados. E para isso, vamos nos referir ao modo como o *Bitcoin Core* faz isso.

Em palavras simples, o *Bitcoin Core* usa dois **'buckets'** para armazenar dados:

 1. **Blocks** armazena metadados descrevendo todos os blocks em uma cadeia.
 2. O **chainstate** armazena o estado de uma cadeia, que são todos os outputs de transações não utilizados e alguns metadados.

Além disso, os blocks são armazenados como arquivos separados no disco. Isso é feito para um objetivo de desempenho: a leitura de um único block não exige que todos sejam carregados *(ou alguns deles)* na memória. Nós não vamos implementar isso.

Nos **blocks**, os pares **chave -> valor** são:

  1. 'b' + hash de bloco de 32 bytes -> registro de índice de bloco.
  2. 'f' + número do arquivo de 4 bytes -> registro de informações do arquivo
  3. 'l' -> número do arquivo de 4 bytes: o último número de arquivo de bloco usado.
  4. 'R' -> booleano de 1 byte: se estamos no processo de reindexação.
  5. 'F' + comprimento do nome do sinalizador de 1 byte + string do nome do sinalizador -> 1 byte booleano: vários sinalizadores que podem estar ativados ou desativados.
  6. 't' + hash de transação de 32 bytes -> registro de índice de transação.

No **chainstate**, os pares **chave -> valor** são:

  1. 'c' + hash de transação de 32 bytes -> registro de saída de transação não utilizada para aquela transação.
  2. 'B' -> hash de bloco de 32 bytes: o hash de bloco até o qual o banco de dados representa as saídas de transação não utilizadas.

Como ainda não temos transações, teremos apenas blocks de blocks. Além disso, como dito acima, iremos armazenar o banco de dados inteiro como um único arquivo, sem armazenar blocos em arquivos separados. Então, não precisaremos de nada relacionado a números de arquivos. Então, esses são os pares **chave -> valor** que usaremos:

 1. Bloco de 32 bytes hash -> estrutura de blocos (serializada)
 2. 'l' -> o hash do último bloco de uma cadeia

Isso é tudo que precisamos saber para começar a implementar o mecanismo de *persistência*.

# Serialização

Como dito anteriormente, em **BoltDB** os valores podem ser apenas do tipo **[]byte**, e queremos armazenar **structs Block** no **DB**. Usaremos *encoding/gob* para serializar as estruturas.

Vamos implementar o método *Serialize* do bloco:

> **Serialização** é o processo de tradução de estruturas de dados ou estado de objeto em um formato que possa ser armazenado (por exemplo, em um arquivo ou buffer de memória, ou transmitido por meio de um enlace de conexão de rede) e reconstruído posteriormente no mesmo ou em outro ambiente computacional.

```golang 
  // Serialize - serializa os dados do block
  func (b *Block) Serialize() []byte {
    /*
      O pacote Bytes implementam funções para a manipulação de slice de bytes.
    */
    var result bytes.Buffer // Retorna um slice de comprimento
    encoder := gob.NewEncoder(&result) // NewEncoder retorna um novo codificador

    /*
      Encode transmite o item de dados representado pelo valor de interface passada,
      garantindo que todas as informações de tipo necessárias tenham sido transmitidas primeiro.
    */
    if err := encoder.Encode(b); err != nil {
      log.Println(err)
    }

    return result.Bytes()
  }
```

Serialize: a princípio, declaramos um *buffer* que armazenará dados **serializados**; então nós inicializamos um codificador gob e codificamos o block; o resultado é retornado como uma *array de bytes*.

Em seguida, precisamos de uma função de *desserialização* que receberá uma *array de bytes* como entrada e retornará um block. Este não será um método, mas uma função independente:

```golang 
// DeserializeBlock - desserializa os bytes e retorna o dados do Block
func DeserializeBlock(d []byte) *Block {
	var block Block

	decoder := gob.NewDecoder(bytes.NewBuffer(d))
	if err := decoder.Decode(&block); err != nil {
		log.Println(err)
	}

	return &block
}
```
Então, fechamos essa parte de serialização!

# Persistência

Vamos começar com a função *NewBlockchain*. Atualmente, ele cria uma nova instância de *Blockchain* e adiciona o *Block Genesis* a ele. O que nós queremos fazer é:

 1. Abrir um arquivo **DB**.
 2. Verificar se há um **blockchain** armazenado nele.
 3. Se houver um **blockchain**:
    1. Crie uma nova **instância do Blockchain**.
    2. Defina a ponta da instância **Blockchain** como o último hash de block armazenado no banco de dados.

  4. Se não houver blockchain existente:
    1. Crie o **Block Genesis**.
    2. Armazenar no **banco de dados**.
    3. Salve o hash do **Block Genesis** como o último hash de block.
    4. Crie uma nova instância **Blockchain** com sua ponta apontando para o **Block Genesis**.

O código vai ficar assim:

```golang
// Criado as constantes
const dbFile = "blockchain_%s.db"
const blocksBucket = "blocks"
const genesisCoinbaseData = "The Times 03/Jan/2009 Chancellor on brink of second bailout for banks"
// Fim constates

func NewBlockchain() *Blockchain {
	var tip []byte

  // Abra o arquivo de dados dbFile no diretório passando.
  // Será criado se não existir.
  db, err := bolt.Open(dbFile, 0600, nil)
  
  // Inicia uma transação de leitura / gravação
	err = db.Update(func(tx *bolt.Tx) error {
    // Todas as chaves em um bucket devem ser exclusivas.
    // Isso definirá o valor da chave no bucket blocksBucket.
    // Isso deve ser criado quando o banco de dados for aberto pela primeira vez.
		b := tx.Bucket([]byte(blocksBucket))

		if b == nil {
      genesis := NewGenesisBlock()
			b, err := tx.CreateBucket([]byte(blocksBucket))
			err = b.Put(genesis.Hash, genesis.Serialize())
			err = b.Put([]byte("l"), genesis.Hash)
			tip = genesis.Hash
		} else {
			tip = b.Get([]byte("l"))
		}

		return nil
	})

	bc := Blockchain{tip, db}

	return &bc
}
```

 Vamos ver pedaço por pedaço:

 ```golang
  db, err := bolt.Open(dbFile, 0600, nil)
 ```
 Esta é uma maneira padrão de abrir um arquivo **BoltDB**. Observe que ele não retornará um erro se não houver tal arquivo.

 ```golang 
  err = db.Update(func(tx *bolt.Tx) error {
    ...
  })
 ```
 No **BoltDB**, as operações com um banco de dados são executadas em uma *transação*. E existem dois tipos de transações: *somente leitura e leitura/gravação*. Aqui, nós abrimos uma transação de *leitura-escrita* **(db.Update (...))**, porque esperamos colocar o **Block Genesis** no banco de dados.

 ```golang 
  // Todas as chaves em um bucket devem ser exclusivas.
  // Isso definirá o valor da chave no bucket blocksBucket.
  // Isso deve ser criado quando o banco de dados for aberto pela primeira vez.
  b := tx.Bucket([]byte(blocksBucket))
  // Se existir
  if b == nil {
    genesis := NewGenesisBlock() //Geramos o Block Genesis
    b, err := tx.CreateBucket([]byte(blocksBucket)) //Criamos um block
    err = b.Put(genesis.Hash, genesis.Serialize()) //Salvamos o block nele
    err = b.Put([]byte("l"), genesis.Hash) //Atualizamos a chave l
    tip = genesis.Hash //Armazenamos o ultimo hash de block na cadeia
  } else {
    tip = b.Get([]byte("l"))//Lemos a chave l
  }
 ```

 Este é o núcleo da função. Aqui, obtemos o *bucket* armazenando nossos blocks: se existir, lemos a chave **l** (o hash do último bloco de uma cadeia) dele; se não existir, geramos o **Block Genesis**, criamos o block, salvamos o block nele e atualizamos a chave **l** armazenando o último hash de block da cadeia.

 Além disso, observe a nova maneira de criar um **Blockchain**:

 ```golang 
  bc := Blockchain{tip, db}
 ```

 Não armazenamos mais todos os blocks, apenas a ponta da cadeia é armazenada. Além disso, armazenamos uma conexão de banco de dados, porque queremos abri-lo uma vez e mantê-lo aberto enquanto o programa está em execução.

 Assim, a estrutura **Blockchain** agora se parece com isso:

 ```golang 
  type Blockchain struct {
    tip []byte
    db  *bolt.DB
  }
 ```

  Próxima coisa que queremos atualizar é o método **AddBlock**: adicionar blocks a uma cadeia agora não é tão fácil quanto adicionar um elemento a uma array. De agora em diante, armazenaremos blocos no banco de dados:

  ```golang 
  // AddBlock - Adicionando um block dentro do Blockchain
  func (bc *Blockchain) AddBlock(data string) {
      var lastHash []byte

      err := bc.db.View(func(tx *bolt.Tx) error {
        b := tx.Bucket([]byte(blocksBucket))
        lastHash = b.Get([]byte("l"))

        return nil
      })

      NewBlock := NewBlock(data, lastHash)

      err = bc.db.Update(func(tx *bolt.Tx) error {
        b := tx.Bucket([]byte(blocksBucket))
        err := b.Put(NewBlock.Hash, NewBlock.Serialize())
        err = b.Put([]byte("l"), NewBlock.Hash)
        bc.tip = NewBlock.Hash

        return nil
      })
  }
  ```
Vamos ver o código pedaço por pedaço:

```golang

err := bc.db.View(func(tx *bolt.Tx) error {
	b := tx.Bucket([]byte(blocksBucket))
	lastHash = b.Get([]byte("l"))

	return nil
})

```

Este é o outro tipo (somente leitura) de transações **BoltDB**. Aqui obtemos o último hash de bloco do banco de dados para usá-lo para extrair um novo hash de block.

```golang 
  newBlock := NewBlock(data, lastHash)
  b := tx.Bucket([]byte(blocksBucket))
  err := b.Put(newBlock.Hash, newBlock.Serialize())
  err = b.Put([]byte("l"), newBlock.Hash)
  bc.tip = newBlock.Hash
```

Após a mineração de um novo block, salvamos sua representação serializada no banco de dados e atualizamos a chave **l**, que agora armazena o hash do novo block.

# Inspecionando o Blockchain

Todos os novos blocks agora são salvos em um banco de dados, para que possamos reabrir um **blockchain** e adicionar um novo block a ele. Mas depois de implementar isso, perdemos um bom recurso: não podemos mais imprimir **blocks de blockchain** porque não armazenamos blocks em um array por mais tempo. Vamos consertar essa falha!

O **BoltDB** permite iterar sobre todas as chaves em um bucket, mas as chaves são armazenadas em ordem de byte, e queremos que os blocks sejam impressos na ordem que eles pegam em um **blockchain**. Além disso, como não queremos carregar todos os blocks na memória (nosso **blockchain DB** pode ser enorme! .. ou simplesmente fingir que poderia), vamos lê-los um por um. Para esse propósito, precisaremos de um *iterador de blockchain*:

```golang 
  // BlockchainIterator - Iterador de blockchain
  type BlockchainIterator struct {
    currentHash []byte
    db          *bolt.DB
  }

```

Um *iterador* será criado toda vez que quisermos iterar sobre blocks em um *blockchain* e ele armazenará o hash de blocks da iteração atual e uma conexão com um banco de dados. Por causa do último, um iterador é logicamente anexado a um *blockchain* (é uma instância **Blockchain** que armazena uma conexão de banco de dados) e, portanto, é criado em um método Blockchain.

```golang 
  func (bc *Blockchain) Iterator() *BlockchainIterator {
	  bci := &BlockchainIterator{bc.tip, bc.db}

	  return bci
  }
```

Observe que um **iterador** aponta inicialmente para a ponta de um *blockchain*, portanto, os blocks serão obtidos de cima para baixo, do mais recente para o mais antigo. De fato, **escolher uma dica significa “votar” para um blockchain**. Um *blockchain* pode ter vários ramos e é o mais longo deles considerado principal. Depois de receber uma dica **(pode ser qualquer block na blockchain)** podemos reconstruir o *blockchain* inteiro e encontrar seu tamanho e o trabalho necessário para construí-lo. Este fato também significa que *uma dica é uma espécie de identificador de uma blockchain*.

O *BlockchainIterator* fará apenas uma coisa: **ele retornará o próximo bloco de um blockchain**.

```golang 

func (i *BlockchainIterator) Next() *Block {

	var block *Block

	err := i.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(blocksBucket))
		encodedBlock := b.Get(i.currentHash)
		block = DeserializeBlock(encodedBlock)

		return nil
	})

	i.currentHash = block.PrevBlockHash

	return block
}
```

Aqui para a parte da DB.


# CLI

Até agora, nossa implementação não forneceu nenhuma interface para interagir com o programa: *simplesmente executamos o NewBlockchain, bc.AddBlock na função principal*. Queremos ter estes comandos:

```sh 

> blockchain_go addblock "Pay 0.031337 for a coffee"
> blockchain_go printchain

```

Todas as operações relacionadas à linha de comando serão processadas pela estrutura da CLI:

```golang 

func (cli *CLI) Run() {
	cli.validateArgs()

	addBlockCmd := flag.NewFlagSet("addblock", flag.ExitOnError)
	printChainCmd := flag.NewFlagSet("printchain", flag.ExitOnError)

	addBlockData := addBlockCmd.String("data", "", "Block Data")

	switch os.Args[1] {
	case "addblock":
		err := addBlockCmd.Parse(os.Args[2:])
	case "printchain":
		err := printChainCmd.Parse(os.Args[2:])
	default:
		cli.printUsage()
		os.Exit(1)
	}

	if addBlockCmd.Parsed() {
		if *addBlockData == "" {
			addBlockCmd.Usage()
			os.Exit(1)
		}
		cli.addBlock(*addBlockData)
	}

	if printChainCmd.Parsed() {
		cli.printChain()
	}

}

```

Estamos usando o pacote de **flag** padrão para analisar os argumentos da linha de comando.

> Os sinalizadores (flag) de linha de comando são uma maneira comum de especificar opções para programas de linha de comando. Go fornece um pacote de sinalizadores que suporta a análise básica de flag na linha de comando.

```golang 

  addBlockCmd := flag.NewFlagSet("addblock", flag.ExitOnError)
  printChainCmd := flag.NewFlagSet("printchain", flag.ExitOnError)
  addBlockData := addBlockCmd.String("data", "", "Block data")

```

 Primeiro, criamos dois subcomandos, *addblock* e *printchain*, depois adicionamos -data ao primeiro. O *printchain* não tem flags.

 ```golang 
 
  switch os.Args[1] {
    case "addblock":
      err := addBlockCmd.Parse(os.Args[2:])
    case "printchain":
      err := printChainCmd.Parse(os.Args[2:])
    default:
      cli.printUsage()
      os.Exit(1)
  }

 ```

 Em seguida, verificamos o comando fornecido pelo subcomando de flag relacionado ao usuário e à análise.

 ```golang 
 
if addBlockCmd.Parsed() {
	if *addBlockData == "" {
		addBlockCmd.Usage()
		os.Exit(1)
	}
	cli.addBlock(*addBlockData)
}

if printChainCmd.Parsed() {
	cli.printChain()
}
 ```

 Em seguida, verificamos quais subcomandos foram analisados ​​e executamos as funções relacionadas.

 ```golang 
 
 func (cli *CLI) addBlock(data string) {
	cli.bc.AddBlock(data)
	fmt.Println("Sucesso!")
}

func (cli *CLI) printChain() {
	bci := cli.bc.Iterator()

	for {

		block := bci.Next()

		fmt.Printf("Prev. hash: %x\n", block.PrevBlockHash)
		fmt.Printf("Data: %s\n", block.Data)
		fmt.Printf("Hash: %x\n", block.Hash)
		pow := NewProotOfWork(block)
		fmt.Printf("PoW: %s\n", strconv.FormatBool(pow.Validate()))
		fmt.Println("------------------------------------")

		if len(block.PrevBlockHash) == 0 {
			break
		}

	}
}

 ```

  Essa peça é muito parecida com a que tínhamos antes. A única diferença é que agora estamos usando um **BlockchainIterator** para iterar sobre blocos em um *blockchain*.

  Também não podemos esquecer de modificar a função principal de acordo:

  ```golang 
  
  func main() {
	  bc := NewBlockchain()
	  defer bc.db.Close()

	  cli := CLI{bc}
	  cli.Run()
  }

  ```

  Da próxima vez, implementaremos **endereços**, **carteiras** e (provavelmente) **transações**.

  







