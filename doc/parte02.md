# Introdução


Na parte 01, construímos uma estrutura de dados muito simples, que é a essência do banco de dados *blockchain*. E nós tornamos possível adicionar blocks a ele com a relação de cadeia entre eles: *cada block está vinculado ao anterior*. Infelizmente, nossa implementação de *blockchain* tem uma falha **significativa**: adicionar blocks à cadeia é fácil e barato. Um dos pilares do *blockchain* e do *Bitcoin* é que adicionar novos blocks *é um trabalho árduo*. Então vamos corrigir essa falha.

# Proof-of-Work ou Prova de Trabalho

Uma idéia chave do *blockchain* é que é preciso realizar algum trabalho duro para colocar dados nele. É esse trabalho árduo que torna o *blockchain* **seguro e consistente**. Além disso, uma recompensa é paga por esse trabalho árduo *(é assim que as pessoas obtêm moedas para mineração)*.

Esse mecanismo é muito semelhante ao da vida real: *é preciso trabalhar muito para obter uma recompensa e sustentar sua vida*. No *blockchain*, alguns participantes **(mineiros)** da rede trabalham para sustentar a rede, adicionar novos blocks a ela e obter uma recompensa pelo seu trabalho. Como resultado de seu trabalho, um block é incorporado ao *blockchain* de maneira segura, o que mantém a estabilidade de todo o banco de dados *blockchain*. Vale a pena notar que quem terminou o trabalho tem que provar isso.

Todo esse mecanismo de *“fazer trabalho duro e provar”* é chamado de *proof-of-work*. É difícil porque requer muito poder computacional: *mesmo os computadores de alto desempenho não podem fazê-lo rapidamente*. Além disso, a dificuldade deste trabalho aumenta de tempos em tempos para manter a taxa de novos blocks em cerca de 6 blocks por hora. Em Bitcoin, o objetivo de tal trabalho é encontrar um hash para um block, que atenda a alguns requisitos. E é esse hash que serve como prova. Assim, encontrar uma prova **é o trabalho real**.

Uma última coisa a notar. Algoritmos de *proof-of-work* devem atender a um requisito: *fazer o trabalho é difícil, mas verificar a prova é fácil*. Geralmente, uma prova é entregue a outra pessoa, portanto, para ela, não deve demorar muito para confirmá-la.

# Hashing

Neste parágrafo, discutiremos o *hashing*. Se você estiver familiarizado com o conceito, poderá ignorar esta parte.

***Hashing*** é um processo de obtenção de um hash para dados especificados. Um hash é uma representação única dos dados em que foi calculado. Uma função hash é uma função que coleta dados de tamanho arbitrário e produz um hash de tamanho fixo. Aqui estão algumas características-chave do hashing:

  1. Dados originais não podem ser restaurados a partir de um hash. Assim, o *hashing* não é criptografia.
  2. Certos dados podem ter apenas um hash e o hash é *exclusivo*.
  3. Alterar um **byte** nos dados de entrada resultará em um hash completamente *diferente*.

    "I like donuts" => SHA256(...) => f80667f6efd44484c2569opkef84012...

As funções de *hashing* são amplamente usadas para verificar a consistência dos dados. Alguns fornecedores de software publicam somas de verificação, além de um pacote de software. Depois de baixar um arquivo, você pode alimentá-lo para uma função de *hashing* e comparar o hash produzido com o fornecido pelo desenvolvedor do software.

No *blockchain*, o *hashing* é usado para garantir a consistência de um block. Os dados de entrada para um algoritmo de hash contém o hash do bloco anterior, tornando impossível *(ou, pelo menos, bastante difícil)* modificar um block na cadeia: *é preciso recalcular seu hash e hashes de todos os blocos após ele*.

# Hashcash

Bitcoin usa ***Hashcash****, *um algoritmo de proof-of-work que foi desenvolvido inicialmente para evitar spam de e-mail*.

> ***Hashcash*** é um sistema de prova de trabalho usado para limitar spam de e-mail e ataques de negação de serviço, e mais recentemente ficou conhecido por seu uso em bitcoin (e outras criptomoedas) como parte do algoritmo de mineração.

Pode ser dividido nas seguintes etapas:

1. Pegue alguns dados públicos conhecidos (em caso de e-mail, o endereço de e-mail do destinatário; no caso do Bitcoin, os cabeçalhos de block).
2. Adicione um contador a ele. O contador começa em 0.
3. Obtenha um hash da combinação de dados + contador.
4. Verifique se o hash atende a certos requisitos.
   1. Se isso acontecer, você está feito.
   2. Se isso não acontecer, aumente o contador e repita as etapas 3 e 4.

Assim, esse é um algoritmo de força bruta: você altera o contador, calcula um novo hash, verifica, incrementa o contador, calcula um hash etc. Por isso, **é computacionalmente caro**.

Agora, vamos examinar mais de perto os requisitos que um hash precisa atender. Na implementação original do *Hashcash*, o requisito soa como *“os primeiros 20 bits de um hash devem ser zeros”*. Em Bitcoin, o requisito é ajustado de tempos em tempos, porque, por padrão, um bloco deve ser gerado a cada 10 minutos, apesar do poder computacional aumentar com o tempo e cada vez mais mineiros se juntarem à rede.

Para demonstrar esse algoritmo, peguei os dados do exemplo anterior ('I like donuts') e encontrei um hash que começa com 3 bytes zero:

    "I like donutsca07ca" => SHA256(...) => 000000258acdv2fd31cb82082c4695...

**ca07ca** é o valor hexadecimal do contador, que é 13240266 no sistema decimal.

# Implementação

Ok, acabamos com a teoria, vamos escrever código! Primeiro, vamos definir a dificuldade da mineração:

```golang 
  const targetBits = 24
```

Em Bitcoin, ***“target bits ou bits alvo”*** é o cabeçalho do bloco que armazena a dificuldade na qual o block foi extraído. Não implementamos um algoritmo de ajuste de meta, por enquanto, para que possamos definir a dificuldade como uma constante global.

**24** é um número arbitrário, nosso objetivo é ter um alvo que tenha menos de 256 bits na memória. E queremos que a diferença seja significativa o suficiente, mas não muito grande, porque quanto maior a diferença, mais difícil é encontrar um hash adequado.

```golang 
/*
  -> 'big': Pacote de implementa aritmética de precisão arbitrária (números grandes).
*/
type ProofOfWork struct {
  block *Block
  target *big.Int
}
// NewProotOfWork - cria um nova prova de trabalho
func NewProotOfWork(b *Block) *ProofOfWork {
  // NewInt aloca e retorna um novo conjunto Int para target.
  target := big.NewInt(1)
  // Lsh define returno como valor absoluto = (target << uint(256-targetBits)).
  target.Lsh(target, uint(256-targetBits))
  // Referencia pow como ponteiro para a memoria de ProofOfWork passando os argumentos esperado
	pow := &ProofOfWork{b, target}
	return pow
}
```

Aqui crie a estrutura *ProofOfWork* que contém um ponteiro para um *block* e um ponteiro para um destino. **“target”** é outro nome para o requisito descrito no parágrafo anterior. Usamos um número inteiro grande devido à maneira como comparamos um hash ao destino: convertemos um hash em um número inteiro grande e verificamos se ele é menor que o alvo.

Na função **NewProofOfWork**, inicializamos um *big.Int* com o valor de 1 e movemos para esquerda por *256 - targetBits* bits. 256 é o comprimento de um hash **SHA-256** em bits, e é o algoritmo hash **SHA-256** que vamos usar. A representação hexadecimal do alvo é:

    10000000000000000000000000000000000000000000000000000000000

E ocupa 29 bytes na memória. E aqui está sua comparação visual com os hashes dos exemplos anteriores:

    0fac49161af82ed938add1d8725835cc123a1a87b1b196488360e58d4bfb51e3
    0000010000000000000000000000000000000000000000000000000000000000
    0000008b0f41ec78bab747864db66bcb9fb89920ee75f43fdaaeb5544f7f76ca

O primeiro hash **(calculado em 'I like donuts')** é maior que o alvo, portanto, não é uma prova válida de trabalho. O segundo hash **(calculado em 'I like de donutsca07ca')** é menor que o alvo, portanto, é uma prova válida.

Você pode pensar em um destino como o limite superior de um intervalo: *se um número (um hash) for menor que o limite, ele é válido e vice-versa*. Diminuir o limite resultará em menos números válidos e, portanto, mais trabalho difícil será necessário para encontrar um válido.

Agora, precisamos dos dados para o hash. Vamos prepará-lo:

```golang 
// Preparando os dados para o hash
func (pow *ProofOfWork) prepareData(nonce int) []byte {
	data := bytes.Join(
		[][]byte{
			pow.block.PrevBlockHash,
			pow.block.Data,
			IntToHex(pow.block.Timestamp),
			IntToHex(int64(targetBits)),
			IntToHex(int64(nonce)),
		},
		[]byte{},
	)

	return data
}

// IntToHex - Converte de Inteiro para Hexadecimal e retorna um array de bytes
func IntToHex(x int64) []byte {
	return []byte(strconv.FormatInt(x, 16))
}
```

Esta peça é direta: nós apenas mesclamos campos de block com o target e o nonce. **nonce** (O "nonce" num block bitcoin é um campo de 32 bits cujo valor é configurado de forma que o hash de um block irá conter uma série de zeros) aqui é o contador da descrição de Hashcash acima, este é um termo criptográfico.

Ok, todas as preparações estão feitas, vamos implementar o núcleo do algoritmo Pow (prova de trabalho):

```golang 

// Run - Núcleo do algoritmo da prova de trabalho
func (pow *ProofOfWork) Run() (int, []byte) {
	var hashInt big.Int
	var hash [32]byte
	maxNonce := math.MaxInt64
	nonce := 0

	fmt.Printf("Minerando o block... \"%s\"\n", pow.block.Data)

	for nonce < maxNonce {
		data := pow.prepareData(nonce)
		hash = sha256.Sum256(data)
    fmt.Printf("\r%x", hash)
    // inclue a hash dentro da variavel hashInt
		hashInt.SetBytes(hash[:])
    // Faz a comparação com variavel target
		if hashInt.Cmp(pow.target) == -1 {
			break
		} else {
			nonce++
		}
	}
	fmt.Print("\n\n")
	return nonce, hash[:]
}
```


Primeiro, nós inicializamos as variáveis: **hashInt** é a representação inteira do hash; **nonce** é o contador. Em seguida, executamos um loop 'infinito': ele é limitado por **maxNonce**, que é igual a **math.MaxInt64**; *isso é feito para evitar um possível estouro de nonce*. Embora a dificuldade de nossa implementação de **PoW** seja baixa demais para o contador transbordar, ainda é melhor ter essa verificação, apenas por precaução.

No loop nós:
  1. Prepare dados.
  2. Hash com SHA-256.
  3. Converte o hash em um inteiro grande.
  4. Compara o inteiro com o target.

Tão fácil quanto foi explicado anteriormente. Agora podemos remover o método *SetHash* do Block e modificar a função *NewBlock*:

```golang 
// NewBlock - Criando e retornando o Block
func NewBlock(data string, prevBlockHash []byte) *Block {
	block := &Block{
		time.Now().Unix(),
		[]byte(data),
		prevBlockHash,
		[]byte{},
		0,
	}
  // Criando uma prova de trabalho
  pow := NewProotOfWork(block)
  // Criando o núcleo do algoritmo da prova de trabalho
	nonce, hash := pow.Run()

	block.Hash = hash[:]
	block.Nonce = nonce

	return block
}
```

Aqui você pode ver que *nonce* é salvo como uma propriedade *Block*. Isso é necessário porque o *nonce* é necessário para verificar uma prova. A estrutura do bloco agora parece assim:

```golang 
  // Block - estrutura de um block
  type Block struct {
    Timestamp     int64
    Data          []byte
    PrevBlockHash []byte
    Hash          []byte
    Nonce         int // verificar a prova
  }
```

Há mais uma coisa a fazer: permitir a validação da *prova de trabalhos*.

```golang 

// Validate - validando a prova de trabalhos
func (pow *ProofOfWork) Validate() bool {
	var hashInt big.Int
	// Preparando a data
	data := pow.prepareData(pow.block.Nonce)
	// Criando a hash
	hash := sha256.Sum256(data)
	// Incluindo o array de bytes ao hashInt
	hashInt.SetBytes(hash[:])
	// Fazendo a validação se é valida
	isValid := hashInt.Cmp(pow.target) == -1

	return isValid
}
```

E é aqui que precisamos do nonce salvo.

Vamos verificar mais uma vez que tudo está bem:

```golang 
func main() {
	bc := NewBlockchain()

	bc.AddBlock("Enviando 1 BTC para Leonardo")
	bc.AddBlock("Enviando 2 BTC para Diogo")

	for _, block := range bc.blocks {
		fmt.Printf("Prev. hash: %x\n", block.PrevBlockHash)
		fmt.Printf("Data: %s\n", block.Data)
		fmt.Printf("Hash: %x\n", block.Hash)
		// Entra aqui a validação
		pow := NewProotOfWork(block)
		fmt.Printf("PoW: %s\n", strconv.FormatBool(pow.Validate()))
		// Termina aqui a validação
		fmt.Println("------------------------------------")
	}
}
```

# Conclusão

Nosso *blockchain* está um passo mais próximo de sua arquitetura atual: 

* *adicionar blocos agora exige muito trabalho, portanto a mineração é possível*. 
	
Mas ainda faltam alguns recursos cruciais: 

* o banco de dados *blockchain* não é **persistente**, não há **carteiras**, **endereços**, **transações** e não há **mecanismo de consenso**. Todas essas coisas vamos implementar em artigos futuros e, por enquanto, feliz mineração!